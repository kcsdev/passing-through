00dV<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1760;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2013-07-19 19:25:42";s:13:"post_date_gmt";s:19:"2013-07-19 19:25:42";s:12:"post_content";s:5619:"<h3>1. Terms</h3>
By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.
<h3>2. Use License</h3>
<ol type="a">
	<li>Permission is granted to temporarily download one copy of the materials (information or software) on Jobify's web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
<ol type="i">
	<li>modify or copy the materials;</li>
	<li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>
	<li>attempt to decompile or reverse engineer any software contained on Jobify's web site;</li>
	<li>remove any copyright or other proprietary notations from the materials; or</li>
	<li>transfer the materials to another person or "mirror" the materials on any other server.</li>
</ol>
</li>
	<li>This license shall automatically terminate if you violate any of these restrictions and may be terminated by Jobify at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</li>
</ol>
<h3>3. Disclaimer</h3>
<ol type="a">
	<li>The materials on Jobify's web site are provided "as is". Jobify makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Jobify does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.</li>
</ol>
<h3>4. Limitations</h3>
In no event shall Jobify or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Jobify's Internet site, even if Jobify or a Jobify authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
<h3>5. Revisions and Errata</h3>
The materials appearing on Jobify's web site could include technical, typographical, or photographic errors. Jobify does not warrant that any of the materials on its web site are accurate, complete, or current. Jobify may make changes to the materials contained on its web site at any time without notice. Jobify does not, however, make any commitment to update the materials.
<h3>6. Links</h3>
Jobify has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Jobify of the site. Use of any such linked web site is at the user's own risk.
<h3>7. Site Terms of Use Modifications</h3>
Jobify may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.
<h3>8. Governing Law</h3>
Any claim relating to Jobify's web site shall be governed by the laws of the State of New York without regard to its conflict of law provisions.

General Terms and Conditions applicable to Use of a Web Site.
<h2>Privacy Policy</h2>
Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.
<ul>
	<li>Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.</li>
	<li>We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.</li>
	<li>We will only retain personal information as long as necessary for the fulfillment of those purposes.</li>
	<li>We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</li>
	<li>Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.</li>
	<li>We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</li>
	<li>We will make readily available to customers information about our policies and practices relating to the management of personal information.</li>
</ul>
We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.";s:10:"post_title";s:20:"Terms and Conditions";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:20:"terms-and-conditions";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2015-11-02 08:16:44";s:17:"post_modified_gmt";s:19:"2015-11-02 06:16:44";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:47:"http://demo.astoundify.com/jobify/?page_id=1760";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}