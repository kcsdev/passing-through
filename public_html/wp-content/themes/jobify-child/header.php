<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Jobify
 * @since Jobify 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<meta name="viewport" content="initial-scale=1">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/font/font.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/colorbox.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick.css" rel="stylesheet" type="text/css">
    
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/source/vendor/html5.js" type="text/javascript"></script>
	<![endif]-->

	<?php wp_head(); ?>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/register.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css">
    
</head>

<body <?php body_class(); ?> id="top">
<?php $fields = get_fields(get_the_ID());?>

	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header header_2 <?php //echo 'header_'.$fields['header']?>" role="banner">
			<div class="container">
			<div class="top_header">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="site-branding">
					<?php $header_image = get_header_image(); ?>
					<h1 class="site-title">
						<?php if ( ! empty( $header_image ) ) : ?>
							<img src="<?php echo $header_image ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" />
						<?php endif; ?>
					</h1>
				</a>
                <?php //if($fields['header'] == 2): // if select header two?>
				<div class="header_logo">
                	<?php if(!empty($fields['second_logo'])): 
							//echo '<img src="'.$fields['second_logo'].'" width="auto" alt=""/>';
						  endif?>
                          <a href="http://www.ismm.co.uk"><img src="http://passingthroughjobs.com/wp-content/uploads/2015/10/header-logo-2.png"></a>
                </div>
                <nav id="site-navigation" class="site-primary-navigation slide-left">
					<a href="#" class="primary-menu-toggle"><i class="icon-cancel-circled"></i> <span><?php _e( 'Close', 'jobify' ); ?></span></a>
					<?php get_search_form(); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'header_menu_two', 'menu_class' => 'nav-menu-primary' ) ); ?>
				</nav>
                <div class="header_two_btn">
                	<?php //if(!empty($fields['news'])){?><a href="http://passingthroughjobs.com/news/<?php //echo $fields['news']?>" class="btn blue">News</a><?php //}?>
                    <?php //if(!empty($fields['blog'])){?><a href="http://passingthroughjobs.com/blog/<?php echo $fields['blog']?>" class="btn orange"><strong>Blog</strong></a><?php //}?>
                </div>
                <?php //else: // display default header?>
                	<!--<nav id="site-navigation" class="site-primary-navigation slide-left">
                        <a href="#" class="primary-menu-toggle"><i class="icon-cancel-circled"></i> <span><?php _e( 'Close', 'jobify' ); ?></span></a>
                        <?php //get_search_form(); ?>
                        <?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu-primary' ) ); ?>
                    </nav>-->
                <?php //endif;?>

				<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<a href="#" class="primary-menu-toggle in-header"><i class="icon-menu"></i></a>
				<?php endif; ?>
			</div>
			</div>
		</header><!-- #masthead -->

		<div id="main" class="site-main">
<?php //print_r($fields);?>