<?php
/**
 * Search
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header('blog'); ?>

	<div id="primary" class="content-area">
		<div id="content" class="container" role="main">

			<div class="page-header">
				<h1 class="page-title"><?php echo get_search_query(); ?></h1>
	
			</div>
			<div class="blog-archive row">
				<div class="entry col-md-<?php echo is_active_sidebar( 'sidebar-blog' ) ? '8' : '12'; ?> col-xs-12">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
                        <?php if ( get_post_type( get_the_ID() ) == 'post') {?>
							<?php get_template_part( 'content', get_post_format() ); ?>
                            <?php }?>
						<?php endwhile; ?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>

				<?php get_sidebar(); ?>
			</div>

		</div><!-- #content -->

		<?php do_action( 'jobify_loop_after' ); ?>
	</div><!-- #primary -->
</div>
<?php get_footer(); ?>