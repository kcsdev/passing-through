<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package Jobify
 * @since Jobify 1.0
 */
?>

		
<?php if ( jobify_theme_mod( 'jobify_cta', 'jobify_cta_display' ) ) : ?>
		<div class="footer-cta">
			<div class="container">
				<?php echo wpautop( jobify_theme_mod( 'jobify_cta', 'jobify_cta_text' ) ); ?>
			</div>
		</div>
		<?php endif; ?>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( is_active_sidebar( 'widget-area-footer' ) ) : ?>
			<div class="footer-widgets">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'widget-area-footer' ); ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<div class="copyright">
				<div class="container">
					<div class="site-info">
						<?php echo apply_filters( 'jobify_footer_copyright', sprintf( __( '&copy; %1$s %2$s &mdash; All Rights Reserved', 'jobify' ), date( 'Y' ), get_bloginfo( 'name' ) ) ); ?>
					</div><!-- .site-info -->

					<a href="#top" class="btt"><i class="icon-up-circled"></i></a>

					<?php
						if ( has_nav_menu( 'footer-social' ) ) :
							$social = wp_nav_menu( array(
								'theme_location'  => 'footer-social',
								'container_class' => 'footer-social',
								'items_wrap'      => '%3$s',
								'depth'           => 1,
								'echo'            => false,
								'link_before'     => '<span class="screen-reader-text">',
								'link_after'      => '</span>',
							) );

							echo strip_tags( $social, '<a><div><span>' );
						endif;
					?>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div>
    </div><!-- #page -->
    
   <input type="hidden" value="<?php echo get_home_url(); ?>" id="home_url" />    
   <!-- <p>This is the main content. To display a lightbox click <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">here</a></p>
        <div id="light" class="white_content"><p>This is the lightbox content.</p> <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a></div>
        <div id="fade" class="black_overlay"></div>-->
    
	<?php wp_footer(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.min.js" type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/magnific-popup.min.js" type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.min.js" type="text/javascript"></script>
	<script>
	
$(document).ready(function(){
	var wd = $(window).width();
	if ((navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) && wd < 568) {
		//alert('test');
		//$('.application_button.button').css({'line-height':'26px', 'padding':'20px 20px 15px;'});
	}
    
    $('.job_application input.button').removeClass('application_button');
    $('.job_application input.button').css({
"padding": "5px 20px",
    "color": "#fff",
    "font-size": "21.94px",
    "font-family": "'HelveticaNeueLTPro-MdCn'",
    "background": "#ff6600",
    "border-radius": "20px",
    "font-weight": "normal",
    "box-shadow": "0 3px 3px #828282",
    "border": "none",
    "line-height": "22px",
    "text-transform": "uppercase",
    "display": "inline-block",
    "position": "absolute",
    "top": "38%",
    "transform": "translateY(-50%)"
});
    
    $('.job_application input.button').click(function(){
        console.log('click');
            $('li.sin_line:eq(1) .icon_profile').click();
    });
    
    
    	/* poplate for qa page */
	$(".inline2").colorbox({inline:true, width:'90%',height:'90%', maxWidth:"100%", maxHeight:"90%" });
	
	$(".poplate, .poplate a").colorbox({inline:true,innerWidth:"90%",maxWidth:"100%", maxHeight:"90%"});
	
  //console.log($(".flexslider5"));
    /* slider home page*/
	//slider2 
    setTimeout(function(){
        $('.wpb_gallery.wpb_content_element.vc_clearfix.slider-video').css({"visibility": "visible"});
    },100);
}); 

        
function display_msg(){
	alert('teswt');
}
	</script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js" type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/ajex_data.js" type="text/javascript"></script>
    
    
    <script>

	(function($){
				
				$.mCustomScrollbar.defaults.theme="inset"; //set "inset" as the default theme
				$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
				
				/*$("body").mCustomScrollbar({
					axis:"yx",
					scrollbarPosition:"outside"
				});*/
				
				$(".box_gray,.brand_list, .support_tab, ul.job_listings, .reg-step4 .inner_block").mCustomScrollbar();
				$('body').mCustomScrollbar({
					advanced:{ autoScrollOnFocus: "string" },
				});
				
				
		})(jQuery);
		$(window).load(function(e) {
			$.mCustomScrollbar.defaults.theme="inset"; //set "inset" as the default theme
			$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
            $("ul.job_listings").mCustomScrollbar();
        });
		
		<?php if(isset($_REQUEST['orderbyjob'])){?>
			jQuery(document).ready(function(e) {
                jQuery('.sin_line:first .tab_title a').click();
            });
				
		<?php }?>
	</script>
   
</body>
</html>