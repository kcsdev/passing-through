<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package Jobify
 * @since Jobify 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'row' ); ?>>
	
		<?php if ( is_single() ) : ?>
			<h2 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
			<h2 class="entry-title">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h2>
		<?php endif; ?>
		
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="entry-feature">
				<?php the_post_thumbnail( 'fullsize' ); ?>
			</div>
		<?php endif; ?>

		<div class="entry-summary">
			<?php if ( is_singular() ) : ?>
				<?php the_content(); ?>

				<?php if ( is_singular() ) : ?>
				<?php the_tags( '<p class="entry-tags"><i class="icon-tag"></i> ' . __( 'Tags:', 'jobify' ) . ' ', ', ', '</p>' ); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'jobify' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
				<?php endif; ?>
			<?php else : ?>
			
			<?php if(is_search()): 
							the_excerpt(); 
				  else: 
							the_content(); 
				  endif;?>
				
			<?php endif; ?>
			<div class="post_meta">
				<div class="post_date">
				<span></span><?php the_time('l, F j, Y')?><br/><?php the_time('g:i a')?>
				</div>
				<div class="post_comment"><span></span><?php comments_number( '0 coments', '1 coments', '% coments' ); ?></div>
				<!--<div class="social"><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/social.png"/></a></div>-->
			</div>
		</div>
</article><!-- #post -->
