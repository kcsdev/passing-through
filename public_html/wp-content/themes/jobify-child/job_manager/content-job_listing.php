<?php global $post; ?>
<?php $highlight = get_post_meta( $post->ID, '_job_highlight', true );
if($highlight == 1){
	$class = 'highlight';
}
else{
	$class= '';
}?>
<?php if ( $apply = get_the_job_application_method() ) :
	if ( $apply->type === 'url' ) {
	    $application_href = $apply->url;
	} elseif ( $apply->type === 'email' ) {
	    $application_href = sprintf( 'mailto:%1$s%2$s', $apply->email, '?subject=' . rawurlencode( $apply->subject )  );
	}
	endif;
	?>
<?php $fields = get_fields($post->ID);
//print_r($fields);?>    
<li id="job_listing-<?php the_ID(); ?>" <?php job_listing_class(); ?> <?php echo apply_filters( 'jobify_listing_data', '' ); ?> data-longitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_long ); ?>">
		<?php $salary = get_post_meta( $post->ID, '_job_location', true );?>
		<div class="position">
			<a href="<?php the_job_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
			<div class="company">
				<?php //the_company_name( '<strong>', '</strong> ' ); ?>
				<?php //the_company_tagline( '<span class="tagline">', '</span>' ); ?>
			</div>
            <a class="read_more" href="#inline_content<?php echo $post->ID?>">READ MORE</a>
		</div>
        <!--<div class="job_title">
        	<label>Job Title:</label>
            <div class="job_rs"><?php echo get_post_meta( $post->ID, '_job_sub_title', true );?></div>
        </div>-->
		<div class="location">
        	<label>Location:</label>
            <div class="job_rs">
			<?php //the_job_location( ); ?>
            <?php echo get_post_meta( $post->ID, '_job_location', true );?>
            </div>
		</div>
		<div class="brand">
        	<label>Brand:</label>
            <div class="job_rs"><?php echo get_post_meta( $post->ID, '_job_brand', true );?></div>
        </div>
        <div class="application">
        <div class="job_application">
			<?php do_action( 'job_application_start', $apply ); ?>
            
            <input type="button" class="application_button button popup-trigger" value="<?php _e( 'Apply Now', 'wp-job-manager' ); ?>" href="#kp"/>
            
            <div class="application_details modal"  id="kp">
                <?php
                    /**
                     * job_manager_application_details_email or job_manager_application_details_url hook
                     */
                    do_action( 'job_manager_application_details_' . $apply->type, $apply );
                ?>
            </div>
            <?php do_action( 'job_application_end', $apply ); ?>
        </div>
			<!--<a class="application_button button" href="<?php echo $application_href; ?>"><?php _e( 'Apply Now', 'wp-job-manager' ); ?></a>-->
		</div>
        <div class="comp_log">
        <?php the_company_logo(); ?>
        </div>
</li>
