<div class="custom_filter">
	<label>sort job by :</label>
    <ul>
	    <li><a href="<?php echo esc_url( add_query_arg( 'orderbyjob', 'recommended', get_page_link() ) );?>" data-ajax="_job_highlight">recommended jobs</a></li>
        <li><a href="<?php echo esc_url( add_query_arg( 'orderbyjob', 'ex_lavel', get_page_link() ) );?>" data-ajax="_ex_lavel">experience level</a></li>
        <li><a href="<?php echo esc_url( add_query_arg( 'orderbyjob', 'location', get_page_link() ) );?>" data-ajax="_job_location">location</a></li>
        <li><a href="<?php echo esc_url( add_query_arg( 'orderbyjob', 'brand', get_page_link() ) );?>" data-ajax="_job_brand">brand</a></li>
    </ul>
</div>
<?php
$args = array('post_type' => 'job_listing', 'orderby' => 'post_date', 'order' => 'DESC', 'post_status' => 'publish','per_page' => get_option( 'job_manager_per_page' ));
$query = new WP_Query($args);
 while ( $query->have_posts() ) : $query->the_post();?>
    
    	  <?php $fields = get_fields(get_the_ID());?>
		
  <div id="inline_content<?php echo get_the_ID()?>" class="job_desc">
    <div class="pop_header">
      
      <ul>
        <li class="print"><a href="#">print <span></span></a></li>
        <li class="save"><a href="<?php echo $fields['download_link']?>" target="_blank">download <span></span></a></li>
        <li class="close"><a href="#">close <span></span></a></li>
      </ul>
    </div>
    <div class="pop_content">
      <div class="pop_left">
        <div class="pop_title">
          <h3><?php the_title(); ?></h3>
        </div>
        <div class="pop_brand">
          <h5>Brand:</h5>
          <p><?php echo get_post_meta( get_the_ID(), '_job_brand', true );?></p>
        </div>
        <div class="pop_desc">
          <h5>Job description:</h5>
          <p><?php echo get_the_content()?> </p>
        </div>
        <div class="pop_requ">
          <h5>Requirements and responsibilities:</h5>
          <p><?php echo $fields['requirement']?></p>
        </div>
        <div class="map">
        	<?php if(!empty($fields['map'])):?><iframe src="<?php echo $fields['map']?>" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe><?php endif;?>
        </div>
      </div>
      <div class="pop_right">
        <div class="pop_location">
          <h5>Location:</h5>
          <p><?php echo get_post_meta( get_the_ID(), '_job_location', true );?></p>
        </div>
        <div class="loc_gallary location_slider">
        	<?php for($i=0; $i<sizeof($fields['location_images']); $i++){
				echo '<img src="'.$fields['location_images'][$i]['image'].'" alt="slide"/>';
			}?>
        </div>
         <div class="pop_skill">
          <h5>Skills and abilities:</h5>
          <p><?php echo $fields['skills']?></p>
        </div>
        <div class="pop_salary">
          <h5>Average Salary:</h5>
          <p><?php echo get_post_meta( get_the_ID(), 'job_salary', true );?></p>
        </div>
        <div class="pop_acc">
          <h5>Accommodation: </h5>
        </div>
        <div class="loc_gallary acc_slider">
        	<?php for($i=0; $i<sizeof($fields['accommodation']); $i++){
				echo '<img src="'.$fields['accommodation'][$i]['images'].'" alt="slide"/>';
			}?>
        </div>
      </div>
    </div>
  </div>
	<?php endwhile; ?>
<ul class="job_listings">
