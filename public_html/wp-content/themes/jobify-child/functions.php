<?php
/**
 * Jobify Child Theme
 *
 * Place any custom functionality/code snippets here.
 *
 * @since Jobify Child 1.0.0
 */


function jobify_child_styles() {
    wp_enqueue_style( 'jobify-child', get_stylesheet_uri() );
    wp_register_style( 'magnific-popup', get_stylesheet_directory_uri() . '/css/magnific-popup.css'  );
    wp_enqueue_style( 'magnific-popup' );
    wp_register_style( 'owl-carousel', get_stylesheet_directory_uri() . '/css/owl.carousel.css'  );
    wp_enqueue_style( 'owl-carousel' );

    // wp_register_script( 'magnific-popup', get_stylesheet_directory_uri() . '/js/magnific-popup.min.js'  );
    // wp_enqueue_script( 'magnific-popup' );
    // wp_register_script( 'owl-carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js'  );
    // wp_enqueue_script( 'owl-carousel' );
}
add_action( 'wp_enqueue_scripts', 'jobify_child_styles', 20 );

function menu_setup() {
	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'header_menu_two'       => __( 'Header Two Menu', 'jobify' ),
	) );
}
add_action( 'after_setup_theme', 'menu_setup' );


add_filter( 'submit_job_form_fields', 'frontend_add_salary_field' );
function frontend_add_salary_field( $fields ) {
  $fields['job']['job_salary'] = array(
    'label'       => __( 'Salary ($)', 'job_manager' ),
    'type'        => 'text',
    'required'    => true,
    'placeholder' => 'e.g. 20000',
    'priority'    => 7
  );
  return $fields;
}

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_salary_field' );
function admin_add_salary_field( $fields ) {
  $fields['job_salary'] = array(
    'label'       => __( 'Salary ($)', 'job_manager' ),
    'type'        => 'text',
    'placeholder' => '',
    'description' => ''
  );
  $fields['_job_sub_title'] = array(
    'label'       => __( 'Sub Title', 'job_manager' ),
    'type'        => 'text',
    'placeholder' => '',
    'description' => ''
  );
  $fields['_job_brand'] = array(
    'label'       => __( 'Brand', 'job_manager' ),
    'type'        => 'text',
    'placeholder' => '',
    'description' => ''
  );
  $fields['_job_highlight'] = array(
    'label'       => __( 'Recommended jobs', 'job_manager' ),
    'type'        => 'checkbox',
    'placeholder' => '',
    'description' => 'Display orange background'
  );
  $fields['_ex_lavel'] = array(
    'label'       => __( 'Experience Level', 'job_manager' ),
    'type'        => 'text',
    'placeholder' => '',
    'description' => 'Enter experience level'
  );
  return $fields;
}


/* add regiseter short code */
require('inc/register.php');
/* job listing shortcode with filter */
require('inc/job_listing.php');
/* job listing ajex with filter */
require('inc/job_listing_ajex.php');
/* sign up question in admin side */
require('inc/signup_question.php');
/* sign up question in admin side */
require('inc/user_profile_custom_fields.php');
/* sign up question in admin side */
require('inc/gallary_slider.php');
?>