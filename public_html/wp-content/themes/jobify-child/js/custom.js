jQuery(document).ready(function(e) {

	var elm = jQuery('.slider-video .wpb_image_grid_ul');
	elm.owlCarousel({
		items : 3,
		nav : true,
		loop : false,
		autoplay:true,
		autoplayTimeout:8000,
		autoplayHoverPause:true,
		margin: 20,
		mouseDrag: true,
		smartSpeed: 600
	});
     function toggleArrows(){ 
        if($(elm).find(".owl-item").last().hasClass('active') && 
           $(elm).find(".owl-item.active").index() == $(elm).find(".owl-item").first().index()){                       
            $(elm).find('.owl-nav .owl-next').addClass("off");
            $(elm).find('.owl-nav .owl-prev').addClass("off"); 
        }
        //disable next
        else if($(elm).find(".owl-item").last().hasClass('active')){
            $(elm).find('.owl-nav .owl-next').addClass("off");
            $(elm).find('.owl-nav .owl-prev').removeClass("off"); 
        }
        //disable previus
        else if($(elm).find(".owl-item.active").index() == $(elm).find(".owl-item").first().index()) {
            $(elm).find('.owl-nav .owl-next').removeClass("off"); 
            $(elm).find('.owl-nav .owl-prev').addClass("off");
        }
        else{
            $(elm).find('.owl-nav .owl-next,.owl-nav .owl-prev').removeClass("off");  
        }
    }
    //turn off buttons if last or first - after change
    $(elm).on('initialized.owl.carousel', function (event) {
        toggleArrows();
    });
     $(elm).on('translated.owl.carousel', function (event) { toggleArrows(); });
     $(elm).find('.owl-nav .owl-prev').addClass("off");


	// PopUp Gallery
	jQuery('.popup-gallery').each(function() { // the containers for all your galleries
	    $(this).magnificPopup({
	        delegate: 'a', // the selector for gallery item
	        type: 'image',
	        gallery: {
	          enabled:true,
	          tCounter: ''
	        }
	    });
	});

	// jQuery('.popup-iframe').magnificPopup({
	// 	type: 'ajax',
	// 	alignTop: true,
	// 	overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
	// });

	jQuery(".btt").click(function(e) {
		e.preventDefault();
		console.log('top');
		jQuery("body").mCustomScrollbar("scrollTo","top");
	});

	jQuery( "body" ).on( "click" , ".mfp-popup-close", function() {
		var magnificPopup = $.magnificPopup.instance; // save instance in magnificPopup variable
		magnificPopup.close(); // Close popup that is currently opened
	});

	jQuery('.popup-video, .slider-video a').magnificPopup({
	    disableOn: 200,
	    type: 'iframe',
	    mainClass: 'mfp-fade',
	    removalDelay: 160,
	    preloader: false,
	    fixedContentPos: false,
	    iframe: {
		  markup: '<div class="mfp-iframe-scaler">'+
		            '<div class="mfp-popup-close">×</div>'+
		            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
		          '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
		}
	});
	
	
	/* add form form register page*/
	jQuery('.add_form').click(function(e) {
		
		var no_of_form = parseInt(jQuery('.no_of_form').val())+1;
		jQuery('.no_of_form').val(no_of_form);
		var form_id = '#form'+no_of_form;
            
			var form_html = '<div class="form" id="form'+no_of_form+'"><div class="line"><div class="inner"><h3>Person '+no_of_form+'</h3><input type="button" class="remove_form" value="Remove Person" onclick="remove_form(\''+no_of_form+'\')"></div></div><div class="line"><div class="inner"><label for="fullname" class="fullname">Full Name:</label><input type="text" name="fullname'+no_of_form+'" placeholder="Ioana Popescu" class="require fullname"><p><label for="age" class="age">Age:</label><select class="age" name="age'+no_of_form+'" class="require"><option value="0">Select</option><option>Under 18 / 20 - 22</option><option>Under 18</option><option>20 - 22</option></select><label for="gender" class="gender" >Gender:</label><select name="gender'+no_of_form+'" class="require gender"><option value="0">Select</option><option>Male</option><option>Female</option></select></p></div></div><div class="line"><div class="inner"><label for="english" class="english">English level:</label><select name="english'+no_of_form+'" class="require english"><option value="0">Select</option><option>Beginner</option><option>Intermediate</option><option>Advanced</option></select><p><label for="scndlang" class="scndlang">Second language:</label><select class="scndlang" name="scndlang'+no_of_form+'"><option value="0">Select</option><option>Austrian</option><option>German</option><option>French</option><option>Italian</option><option>Spanish</option><option>Russian</option><option>Greek</option><option>Flemish</option><option>Finnish</option><option>Danish</option><option>Swedish</option><option>Norwegian Arabic</option><option>Other</option></select><label for="level" class="level">Level:</label><select class="level" name="level'+no_of_form+'" class="require"><option value="0">Select</option><option>Beginner</option><option>Intermediate</option><option>Advanced</option></select></p></div></div><div class="line"><div class="inner"><label for="city" class="city">City of Residence:</label><input type="text" name="city'+no_of_form+'" placeholder="Bucharest" class="require city"><p><label for="schooldegree" class="schooldegree">Last school degree:</label><select name="schooldegree'+no_of_form+'" class="require schooldegree"><option value="0">Select</option><option>Baccalaureate Diploma</option><option>University Degree</option><option>Master’s Degree</option><option>Others</option></select><label for="study-type" class="study_type">What did you study?</label><input type="text" name="study_type'+no_of_form+'" placeholder="economics" class="require study_type"></p></div></div><div class="line"><div class="inner"><label for="exp" class="exp">Work experience:</label><select name="exp'+no_of_form+'" class="require"><option value="0">Select</option><option>0-3 years</option><option>3-6 years</option><option>6 -9 years</option><option>More than 9 years </option></select><p><label for="expinsel" class="expinsel">Experience In sales:</label><select name="expinsel'+no_of_form+'" class="require expinsel"><option value="0">Select</option><option>0-3 years</option><option>3-6 years</option><option>6 -9 years</option><option>More than 9 years</option></select><label for="position" class="position">What position?</label><input type="text" name="position'+no_of_form+'" placeholder="client service..." class="require position_input"></p></div></div></div>';
			
			jQuery('.list_form').append(form_html);	
			jQuery('#registration_form').prepend('<input type="hidden" value="'+no_of_form+'" class="form_id" name="form_id[]" id="form_id'+no_of_form+'"/>');
					
     });
	
	
	/* poplate for qa page */
	$(".inline2").colorbox({inline:true, width:'960px',height:'660px', maxWidth:"100%", maxHeight:"90%" });
	
	$(".poplate, .poplate a").colorbox({inline:true,innerWidth:500,maxWidth:"100%", maxHeight:"90%"});
	
	var tab_active = jQuery('.home_tab ul li.active a').attr('href');
	jQuery('.'+tab_active).show();
	

	/* register tab */
	jQuery('.next_step').click(function(e) {
		jQuery('.hide_tab').hide();
		if(jQuery(this).hasClass('sing_person')){
			jQuery('.person_1').hide();
		}
		jQuery(jQuery(this).attr('href')).show();
        return false;
    });
	
	/* custom checkbox  */
	jQuery('.overcheckbox input').click(function(e) {
		
        if(jQuery(this).parents('.overcheckbox').hasClass('check')){
			jQuery(this).parents('.overcheckbox').removeClass('check');
		}
		else{
			jQuery(this).parents('.overcheckbox').addClass('check');
		}
		return false;
    });
	
	/* display next step */
	//.step-penal .next_tab,
	jQuery('.step-penal .next_tab,a.nxt-step').click(function(e) {
		var valid_form = validate_register_form(jQuery('.active_tab').val());
		var href = jQuery(this).attr('href');
		
		if(valid_form == true){
	        jQuery('.hide_tab').hide();
			jQuery(href).show();
			jQuery('.step-penal div').removeClass('active').addClass('inactive');
			jQuery('.step-penal .next_tab[href="'+href+'"]').parent().removeClass('inactive').addClass('active');
				/*$("html, body").animate({
					scrollTop: $(href).offset().top 
				});*/
		}
		else if(valid_form == false){
		
			alert('Please enter require field value');
		}
		jQuery('.active_tab').val(jQuery(this).data('value'));// set active tab value in form
        return false;
    });
	
	/* home page on click tab display content */
    jQuery('.home_tab ul li a').click(function(e) {
		var hrf = jQuery(this).attr('href');
		
		if(jQuery(this).parents('li').hasClass('active')){
			jQuery(this).parents('li').removeClass('active');
			jQuery('.tab_list').hide();
			jQuery('.home_content').show();
		}
		else{
			
			jQuery('.home_content').hide();
			jQuery('.home_tab ul li').removeClass('active');
			jQuery(this).parents('li').addClass('active');
			jQuery('.tab_list').hide();
			jQuery('.'+hrf).show();
		}
		if(hrf == 'job_opening'){
			jQuery('ul.job_listings li.job_listing').each(function(index, element) {
				var heig = jQuery(this).height();
				jQuery(this).find('.comp_log').height(heig);
			});
		}
        return false;
    });
	
	/* on submit registration form */
	jQuery('#registration_form').submit(function(e) {
		var valid_form = validate_register_form(jQuery('.active_tab').val());
		
		if(valid_form == false){
			alert('Please enter require field value');
			return false;
		}
		
    });
	
	/* on select img */
	jQuery('.select_img').click(function(e) {
        jQuery('#my_image_upload').click();
		return false;
    });
	
	/* character limit */
	jQuery('.char_limit').keyup(function(e) {
        var chars = jQuery(this).val().length;
		limit = jQuery(this).attr('maxlength');
		if (chars > limit) {
			src.value = src.value.substr(0, limit);
			chars = limit;
		}
		jQuery(this).parent('.inner').find('.limit span').html( chars );
    });
	
	
	/* display add form button*/		
	jQuery('.group_form').click(function(e) {
        jQuery('.add_from_btn').css('display','inline-block');
    });
	
	/* login form checkbox click */
	jQuery('#rememberme').click(function(e) {
        if(jQuery(this).is(':checked')){
			jQuery(this).parents('.tml-rememberme-wrap').addClass('check');
		}
		else{
			jQuery(this).parents('.tml-rememberme-wrap').removeClass('check');
		}
    });
	
	/* question display detail */
	jQuery('.reg-step4 .block button').click(function(e) {
        jQuery(this).parents('.block').find('.que_detail').slideToggle();
    });
	
	/* select video*/
	jQuery('.upload_video img').click(function(e) {
        jQuery('.video_file').click();
    });
	
	/* display more content for whe we are */
	jQuery('.read-more a').click(function(e) {
		var hide_content = jQuery(this).closest('.who_we_content').find('.wpb_text_column.hide_content');
		if(hide_content.is(':visible')){
			hide_content.slideUp();
			jQuery(this).text('Read More').removeClass('active-read-more');
		}
		else{
        	hide_content.slideDown();
			jQuery(this).text('Show Less').addClass('active-read-more');
		}
		return false;
    });
	
	/* on brand click */
	jQuery('.brands .vc_col-sm-6').click(function(e) {
		//window.location.href();
		window.open(jQuery(this).find('.partner_link').attr('href'), '_blank');
		
    });
	
	/* slider home page*/
	jQuery('.loc_gallary').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
	});
	
	/*  gallery slider  */
	jQuery('.gallery_slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
	});
	
	/*  recommended_jobs  slider  */
	jQuery('.recommended_jobs').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
	});
	
	/* */
	$('.apply_btn').click(function(e) {
		$('.home_tab ul li .icon_profile').click();
        return false;
    });
	
	$('.image_gallary').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  dots: true,
	  arrows: false,
	});
	
});

/* print page */ 
jQuery(document).on('click', '.pop_header .print a', function () {
	window.print();
	return false;
});

/* close buttom click hide popup */
jQuery(document).on('click', '.close a', function () {
	jQuery('.job_desc').hide();
	return false;
});
	
/* textbox on focus remove border red*/
jQuery(document).on('focus', '.require', function () {
	jQuery(this).css('border-color','#ccc');
	jQuery(this).removeClass('field_error');
    
});


/* job listing poplate */
jQuery(document).on('click', '.job_listing .read_more', function () {
	jQuery(jQuery(this).attr('href')).show();
	return false;
});

jQuery(document).on('click', '.tab_list .application_button', function () {
	location.href=jQuery(this).attr('href');
	return false;
});
	 
function remove_form(form_id){
	
	jQuery('#form_id'+form_id).remove();
	jQuery('#form'+form_id).remove();
}

function validate_register_form(step){
	var valid_from = true;
	var rq_cls = '#step'+step;
	jQuery(rq_cls+' .require').each(function(index, element) {       
		
		if(jQuery(this).val() == ''){
			jQuery(this).css('border-color','#f00');
			jQuery(this).addClass('field_error');
			valid_from = false;
		}
	});
	jQuery(rq_cls+' select.require').each(function(index, element) {       
		
		if(jQuery(this).val() == 0){
			jQuery(this).css('border','#f00;');
			jQuery(this).addClass('field_error');
			valid_from = false;
		}
	});
	jQuery(rq_cls+' .overcheckbox.require2').each(function(index, element) {   // step 4    
		
		if(jQuery(this).hasClass('check')){
			jQuery(this).removeClass('field_error');
		}
		else{
			jQuery(this).addClass('field_error');
			valid_from = false;
		}
	});
	var pass = jQuery('.password').val();
	var cpass = jQuery('.cpassword').val();
	if(pass != cpass){
		alert('Password Do Not Match');
		valid_from = 'password not match';
	}
	return valid_from;
}

function show_msg(msg){
	jQuery('.white_content p').text(msg);
	
	// document.getElementById("msg_pop").click();
}
show_msg('prakash testing');
