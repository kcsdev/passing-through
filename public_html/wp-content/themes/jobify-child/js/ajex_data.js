jQuery(document).on("click",".custom_filter ul li a", function(){
	jQuery('div.job_listings').prepend('<div class="loading_job"></div>');
	var home_url = jQuery('#home_url').val();
	
	var filter = jQuery(this).data('ajax');		
	 jQuery.ajax({
	  type:"POST",
	  url: home_url+"/wp-admin/admin-ajax.php", // url required full path......
	  data: {
	  'action':'job_listing_ajex',
	  'fruit' : filter
	  },
	  success:function(data){
		
		jQuery('.job_listings').html(data);
		//jQuery('.job_listings .loading_job').remove();
		$.mCustomScrollbar.defaults.theme="inset"; //set "inset" as the default theme
		$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
		$("ul.job_listings").mCustomScrollbar();
		jQuery('.loc_gallary').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
		});
		jQuery('ul.job_listings li.job_listing').each(function(index, element) {
			var heig = jQuery(this).height();
			jQuery(this).find('.comp_log').height(heig);
		});
	  }
  }); 
  
  return false;
});