<?php
/**
 * Template Name: Full Width
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jobify
 * @since Jobify 1.5
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

			<?php //get_template_part( 'content', 'page' ); ?>
            <?php the_content()?>

		<?php do_action( 'jobify_loop_after' ); ?>

	<?php endwhile; ?>

<?php get_footer(); ?>