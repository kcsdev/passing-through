<?php
/**
 * Template Name: Page With Graybox
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jobify
 * @since Jobify 1.5
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

<div id="primary" class="content-area">
  <div id="content" class="container remove_bg" role="main">
    <div class="wpb_text_column wpb_content_element ">
      <div class="wpb_wrapper">
        <h1>LET YOUR CAREER SHINE</h1>
      </div>
    </div>
    <div class="box_gray">
      <?php get_template_part( 'content', 'page' ); ?>
    </div>
  </div>
  <!-- #content -->
  
  <?php do_action( 'jobify_loop_after' ); ?>
</div>
<!-- #primary -->

<?php endwhile; ?>
<?php get_footer(); ?>
