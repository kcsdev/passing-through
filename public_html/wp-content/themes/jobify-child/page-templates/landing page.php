<?php
/**
 * Template Name: Landing Page
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jobify
 * @since Jobify 1.5
 */
 ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link href="<?php echo get_stylesheet_directory_uri(); ?>/font/font.css" rel="stylesheet" type="text/css">
<?php wp_head(); ?>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="landing_page">
  <?php while ( have_posts() ) : the_post(); ?>
  <?php the_content()?>
 
  <?php endwhile; ?>
  <?php wp_footer(); ?>
</div>
</body>
</html>
