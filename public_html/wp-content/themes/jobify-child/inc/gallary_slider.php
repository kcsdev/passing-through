<?php
add_action( 'init', 'slider_gallary_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function slider_gallary_init() {
	$labels = array(
		'name'               => _x( 'Gallery Slider', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Gallery Slider', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Gallery Slider', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Gallery Slider', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Gallery Slider', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Gallery Slider', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Gallery Slider', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Gallery Slider', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Gallery Slider', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Gallery Slider', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Gallery Slider', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Gallery Slider:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Gallery Slider found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Gallery Slider found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'book' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', )
	);

	register_post_type( 'slider_gallary', $args );
}

/* shortcode */
function int_shortcode_gallery( $atts, $content = "" ) {
	$args = array( 'post_type' => 'slider_gallary', 'post_status' => 'publish', 'posts_per_page' => -1, 'orderby' => 'date' );
	$the_query = new WP_Query( $args );
	
	// The Loop
	if ( $the_query->have_posts() ) :
		echo '<div class="gallery_slider">';
		  while ( $the_query->have_posts() ) : $the_query->the_post();
		  if ( has_post_thumbnail( get_the_ID() ) ) {?>
				<div class="item">	<a href="#display_video<?php echo get_the_ID()?>" class="poplate"><?php echo get_the_post_thumbnail( get_the_ID(), 'full' );?> </a> 
                	<div style="display:none">
                    	<div id="display_video<?php echo get_the_ID()?>">
                        	<?php echo get_the_content()?>
                        </div>
                    </div>
                </div>
            <?php }					
		  endwhile;
	  	echo '</div>';
	endif;

	// Reset Post Data
	wp_reset_postdata();
	//return "content = $content";
}
add_shortcode( 'gallery_slider', 'int_shortcode_gallery' );