<?php

if(isset($_POST['register_user'])){
	extract( $_POST );
	global $error;
	//print_r($_POST);
	
	
	//$username = sanitize_user( $fullname );
	$email    = apply_filters( 'user_registration_email', sanitize_email( $email ) );
	$username = strstr($email, '@', true); 
	
	if ( empty( $email ) ) {
		 $error = new WP_Error( 'validation-error', __( 'Invalid email address.', 'wp-job-manager' ) );
	}

	if ( empty( $username ) ) {
		$username = sanitize_user( current( explode( '@', $email ) ) );
	}

	if ( ! is_email( $email ) ) {
		$error = new WP_Error( 'validation-error', __( "Your email address isn't correct.", 'wp-job-manager' ) );
	}

	if ( email_exists( $email ) ) {
		$error = new WP_Error( 'validation-error', __( 'This email is already registered, please choose another one.', 'wp-job-manager' ) );
	}
  	
	if($password != $cpassword){
		$error = new WP_Error( 'validation-error', __( 'Password do not match.', 'wp-job-manager' ) );
	}
	if( is_wp_error( $error ) && !is_user_logged_in()) {
    	//echo $error->get_error_message();?>
		<script>alert("<?php echo $error->get_error_message()?>"); </script>
	<?php }
	else{
		
	  // Generate the password and create the user
	  if(!is_user_logged_in()){
		  
	  $password = $password;//wp_generate_password( 12, false );
	  $user_id = wp_create_user( $username, $password, $email );
		
		wp_set_current_user($user_id, $loginusername);
        wp_set_auth_cookie($user_id);
        do_action('wp_login', $loginusername);
	  }else{
		  $user_id = get_current_user_id();
	  }
	  // Set the nickname
	  wp_update_user(array( 'ID' => $user_id, 'name' => $username, 'nickname' => $username) );
	  
	  update_user_meta( $user_id, 'active_tab', $active_tab );	
	  update_user_meta( $user_id, 'fullname', $fullname );	
	  update_user_meta( $user_id, 'age', $age );
	  update_user_meta( $user_id, 'gender', $gender );
	  update_user_meta( $user_id, 'english', $english );
	  update_user_meta( $user_id, 'scndlang', $scndlang );
	  update_user_meta( $user_id, 'level', $level );
	  update_user_meta( $user_id, 'city', $city );
	  update_user_meta( $user_id, 'schooldegree', $schooldegree );
	  update_user_meta( $user_id, 'study_type', $study_type );
	  update_user_meta( $user_id, 'exp', $exp );
	  update_user_meta( $user_id, 'expinsel', $expinsel );
	  update_user_meta( $user_id, 'position', $position );
	  update_user_meta( $user_id, 'mobile_no', $position );
			  	
	  update_user_meta( $user_id, 'new_experience', $new_experience );
	  
	  $que_check = serialize($que_check);
	  update_user_meta( $user_id, 'que_check', $que_check );
	  
	  $que_check = serialize($que_answer);
	  update_user_meta( $user_id, 'que_answer', $que_check );
	  
	 if(isset($form_id)){ 
	  for($i=0; $i <= sizeof($form_id); $i++){
		  update_user_meta( $user_id, 'fullname'.$form_id[$i], $_POST['fullname'.$form_id[$i]] );
		  update_user_meta( $user_id, 'age'.$form_id[$i], $_POST['age'.$form_id[$i]] );
		  update_user_meta( $user_id, 'gender'.$form_id[$i], $_POST['gender'.$form_id[$i]] );
		  update_user_meta( $user_id, 'english'.$form_id[$i], $_POST['english'.$form_id[$i]] );
		  update_user_meta( $user_id, 'scndlang'.$form_id[$i], $_POST['scndlang'.$form_id[$i]] );
		  update_user_meta( $user_id, 'level'.$form_id[$i], $_POST['level'.$form_id[$i]] );
		  update_user_meta( $user_id, 'city'.$form_id[$i], $_POST['city'.$form_id[$i]] );
		  update_user_meta( $user_id, 'schooldegree'.$form_id[$i], $_POST['schooldegree'.$form_id[$i]] );
		  update_user_meta( $user_id, 'study_type'.$form_id[$i], $_POST['study_type'.$form_id[$i]] );
		  update_user_meta( $user_id, 'exp'.$form_id[$i], $_POST['exp'.$form_id[$i]] );
		  update_user_meta( $user_id, 'expinsel'.$form_id[$i], $_POST['expinsel'.$form_id[$i]] );
		  update_user_meta( $user_id, 'position'.$form_id[$i], $_POST['position'.$form_id[$i]] );
	  }
	  	  $data_form = serialize($form_id);
		  update_user_meta( $user_id, 'form_id', $data_form );
		  
	 }
	  // Set the role
	  $user = new WP_User( $user_id );
	  if(get_option('job_manager_registration_role')){
		  $user->set_role( get_option( 'job_manager_registration_role' ) );
	  }
	  else{	  
	  	$user->set_role( get_option( 'default_role' ) );
	  }
	
	  // Email the user
		$to = $email;
		$subject = 'Welcome To Passing Through Jobs';
		$body = '<table border="0">
					<tr><td>Your Username:</td><td><strong>' . $username.'</strong></td></tr> 
					<tr><td>Your Password:</td><td><strong>' . $password.'</strong></td></tr> 
					<tr><td>'.esc_url( home_url( '/' ) ).'</td></tr> 
				</table>';
		$headers[] = 'From: Passing Through Jobs <'.$email.'>';
		$headers[] = 'Cc: Passing Through Jobs <'.$email.'>';
		$headers[] = 'Cc: '.$email.''; // note you can just use a simple email address
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $to, $subject, $body , $headers );
		
		$admin_email = get_option( 'admin_email' );
		$subject = 'New User Register Passing Through Jobs';
		$body = '<table border="0">
					<tr><td>Your Username:</td><td><strong>' . $username.'</strong></td></tr> 
					<tr><td>Your Password:</td><td><strong>' . $password.'</strong></td></tr> 
				</table>';
		wp_mail( $admin_email, $subject, $body , $headers );
		
		$_POST['post_id'] = 0;
	if ( isset( $_POST['my_image_upload_nonce'], $_POST['post_id'] ) && wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )) {
			  // The nonce was valid and the user has the capabilities, it is safe to continue.
		  
			  // These files need to be included as dependencies when on the front end.
			  require_once( ABSPATH . 'wp-admin/includes/image.php' );
			  require_once( ABSPATH . 'wp-admin/includes/file.php' );
			  require_once( ABSPATH . 'wp-admin/includes/media.php' );
			  
			  // Let WordPress handle the upload.
			  // Remember, 'my_image_upload' is the name of our file input in our form above.
			  $attachment_id = media_handle_upload( 'my_image_upload', $_POST['post_id'] );
			  
			  update_user_meta( $user_id, 'profile_photoid', $attachment_id );
		  	
		  }
		  
		?>
		<script>alert("Save your profile successfully");</script>
        <?php 
	}

	if ( isset( $_POST['upload_video'], $_POST['post_id'] ) && wp_verify_nonce( $_POST['video_upload_nonce'], 'upload_video' )) {
		// These files need to be included as dependencies when on the front end.
			/*require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );*/
			
			// Let WordPress handle the upload.
			// Remember, 'my_image_upload' is the name of our file input in our form above.
			$attachment_id = media_handle_upload( 'upload_video', $_POST['post_id'] );
			
			update_user_meta( $user_id, 'upload_video', $attachment_id );
	}
}
function int_register_from( $atts ) {
	?>
<?php if(is_user_logged_in()){
	 	$user_id = get_current_user_id();
		$user_info = get_userdata($user_id);
		$single = true; 
		$active_tab = get_user_meta( $user_id, 'active_tab', $single ) == '' ? '1' : get_user_meta( $user_id, 'active_tab', $single );
	  }
	  else{
		  $active_tab = 1;
	  }?>
                
<div class="hide_tab active">
<div class="register-start">

  <div class="block">
    <h3>Individual application</h3>
    <figure><img src="http://passingthroughjobs.com/wp-content/uploads/2015/10/single-man.png" alt="single-man"></figure>
    <a href="#step<?php echo $active_tab?>" class="next_step sing_person">
    <button>Apply Now</button>
    </a> </div>
  <div class="block">
    <h3>Couple application</h3>
    <figure><img src="http://passingthroughjobs.com/wp-content/uploads/2015/10/couple.png" alt="couple"></figure>
    <a href="#step<?php echo $active_tab?>" class="next_step add_form">
    <button>Apply Now</button>
    </a> </div>
  <div class="block">
    <h3 class="gp_app">Group application</h3>
    <figure><img src="http://passingthroughjobs.com/wp-content/uploads/2015/10/group.png" alt="group"></figure>
    <a href="#step<?php echo $active_tab?>" class="next_step group_form">
    <button>Apply Now</button>
    </a> </div>
</div>
</div>

<form method="post" id="registration_form" enctype="multipart/form-data">
<input type="hidden" value="1" class="no_of_form" name="no_of_form"/>
<input type="hidden" value="<?php echo $active_tab?>" class="active_tab" name="active_tab"/>
<?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
<?php wp_nonce_field( 'upload_video', 'video_upload_nonce' ); ?>

<!-- step 1 -->
<div id="step1" class="hide_tab">
<div class="reg-step1">
  <div class="sub-header">
		<div class="step-penal">
			<div class="active"><a href="#step1" class="next_tab" data-value="1"><h5><span>step 1</span> out of 5</h5><p>Getting to know you</p></a></div>
			<div class="inactive"><a href="#step2" class="next_tab" data-value="2"><h5><span>step 2</span> out of 5</h5><p>Tell us more </p></a></div>
			<div class="inactive"><a href="#step3" class="next_tab" data-value="3"><h5><span>step 3</span> out of 5</h5><p>Your sALes video</p></a></div>
			<div class="inactive"><a href="#step4" class="next_tab" data-value="4"><h5><span>step 4</span> out of 5</h5><p>Check list</p></a></div>
			<div class="inactive"><a href="#step5" class="next_tab"  data-value="5"><h5><span>step 5</span> out of 5</h5><p>Online interview</p></a></div>
		</div>
	</div>
  <div class="list_form">
    <div class="form">
    <div class="line person_1"><div class="inner"><h3>Person 1</h3>
    </div>
    </div>
      <div class="line">
        <div class="inner">
          <label for="fullname">Full Name:</label>
          <input type="text" name="fullname" placeholder="Ioana Popescu" class="require" value="<?php echo get_user_meta( $user_id, 'fullname', $single )?>">
          <p>
            <label for="age">Age:</label>
            <input type="number" name="age" class="require" min="1" value="<?php echo get_user_meta( $user_id, 'age', $single ) ?>" placeholder="Your are">
            
            <label for="gender">Gender:</label>
            <select name="gender" class="require">
              <option value="0">Select</option>
              <option <?php echo get_user_meta( $user_id, 'gender', $single ) == 'Male' ? 'selected="selected"' : ''?>>Male</option>
              <option <?php echo get_user_meta( $user_id, 'gender', $single ) == 'Female' ? 'selected="selected"' : ''?>>Female</option>
            </select>
            
          </p>
        </div>
      </div>
      <div class="line">
        <div class="inner">
          <label for="english">English level:</label>
          <select name="english" class="require">
          <option value="0">Select</option>
            <option <?php echo get_user_meta( $user_id, 'english', $single ) == 'Beginner' ? 'selected="selected"' : ''?>>Beginner</option>
            <option <?php echo get_user_meta( $user_id, 'english', $single ) == 'Intermediate' ? 'selected="selected"' : ''?>>Intermediate</option>
            <option <?php echo get_user_meta( $user_id, 'english', $single ) == 'Advanced' ? 'selected="selected"' : ''?>>Advanced</option>
          </select>
          <p>
            <label for="scndlang">Second language:</label>
            <select name="scndlang" class="require" >
            <option value="0">Select</option>
			<option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Arabic' ? 'selected="selected"' : ''?>>Arabic</option>
              <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Austrian' ? 'selected="selected"' : ''?>>Austrian</option>
			  <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Danish' ? 'selected="selected"' : ''?>>Danish</option>
              <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'German' ? 'selected="selected"' : ''?>>German</option>
			  <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Greek' ? 'selected="selected"' : ''?>>Greek</option>
			  <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Flemish' ? 'selected="selected"' : ''?>>Flemish</option>
              <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Finnish' ? 'selected="selected"' : ''?>>Finnish</option>
              <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'French' ? 'selected="selected"' : ''?>>French</option>
              <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Italian' ? 'selected="selected"' : ''?>>Italian</option>
			  <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Norwegian' ? 'selected="selected"' : ''?>>Norwegian</option>
			  <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Russian' ? 'selected="selected"' : ''?>>Russian</option>
              <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Spanish' ? 'selected="selected"' : ''?>>Spanish</option>
			  <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Swedish' ? 'selected="selected"' : ''?>>Swedish</option>
              <option <?php echo get_user_meta( $user_id, 'scndlang', $single ) == 'Other' ? 'selected="selected"' : ''?>>Other</option>
            </select>
           
            
            <label for="level">Level:</label>
            <select name="level" class="require">
             	<option value="0">Select</option>
                <option <?php echo get_user_meta( $user_id, 'level', $single ) == 'Beginner' ? 'selected="selected"' : ''?>>Beginner</option>
            	<option <?php echo get_user_meta( $user_id, 'level', $single ) == 'Intermediate' ? 'selected="selected"' : ''?>>Intermediate</option>
           		<option <?php echo get_user_meta( $user_id, 'level', $single ) == 'Advanced' ? 'selected="selected"' : ''?>>Advanced</option>
            </select>
           
          </p>
        </div>
      </div>
      <div class="line">
        <div class="inner">
          <label for="city">City of Residence:</label>
          <input type="text" name="city" placeholder="Bucharest" class="require" value="<?php echo get_user_meta( $user_id, 'city', $single )?>">
          <p>
            <label for="schooldegree">Last school degree:</label>
            <select name="schooldegree" class="require">
             	<option value="0">Select</option>
                <option <?php echo get_user_meta( $user_id, 'schooldegree', $single ) == 'Baccalaureate Diploma' ? 'selected="selected"' : ''?>>Baccalaureate Diploma</option>
            	<option <?php echo get_user_meta( $user_id, 'schooldegree', $single ) == 'University Degree' ? 'selected="selected"' : ''?>>University Degree</option>
           		<option <?php echo get_user_meta( $user_id, 'schooldegree', $single ) == 'Master’s Degree' ? 'selected="selected"' : ''?>>Master’s Degree</option>
                <option <?php echo get_user_meta( $user_id, 'schooldegree', $single ) == 'Others' ? 'selected="selected"' : ''?>>Others</option>
            </select>
            
            <label for="study-type">What did you study?</label>
            <input type="text" name="study_type" placeholder="economics" class="require" value="<?php echo get_user_meta( $user_id, 'study_type', $single )?>">
          </p>
        </div>
      </div>
      <div class="line">
        <div class="inner">
          <label for="exp">Work experience:</label>
          <select name="exp" class="require">
          	<option value="0">Select</option>
            <option <?php echo get_user_meta( $user_id, 'exp', $single ) == '0-3 years' ? 'selected="selected"' : ''?>>0-3 years</option>
            <option <?php echo get_user_meta( $user_id, 'exp', $single ) == '3-6 years' ? 'selected="selected"' : ''?>>3-6 years</option>
            <option <?php echo get_user_meta( $user_id, 'exp', $single ) == '6 -9 years' ? 'selected="selected"' : ''?>>6 -9 years</option>
            <option <?php echo get_user_meta( $user_id, 'exp', $single ) == 'More than 9 years ' ? 'selected="selected"' : ''?>>More than 9 years </option>
          </select>
          <p>
            <label for="expinsel">Experience In sales:</label>
            <select name="expinsel" class="require">
            	<option value="0">Select</option>
              <option <?php echo get_user_meta( $user_id, 'expinsel', $single ) == '0-3 years' ? 'selected="selected"' : ''?>>0-3 years</option>
              <option <?php echo get_user_meta( $user_id, 'expinsel', $single ) == '3-6 years' ? 'selected="selected"' : ''?>>3-6 years</option>
              <option <?php echo get_user_meta( $user_id, 'expinsel', $single ) == '6 -9 years' ? 'selected="selected"' : ''?>>6 -9 years</option>
              <option <?php echo get_user_meta( $user_id, 'expinsel', $single ) == 'More than 9 years' ? 'selected="selected"' : ''?>>More than 9 years</option>
            </select>
            <label for="position">What position?</label>
            <input type="text" name="position" placeholder="client service..." class="require" value="<?php echo get_user_meta( $user_id, 'position', $single )?>">
          </p>
        </div>
      </div>
      <?php  if(!is_user_logged_in()){?>
      <div class="line">
	      <div class="inner">
          <label for="fullname">Password:</label>
          <input type="password" name="password" placeholder="Enter Password" class="require password">
          <p class="pass"><label for="cpassword">Confirm Password:</label>
          <input type="password" name="cpassword" placeholder="Confirm Password" class="require cpassword"></p>
        </div>
        </div>
        <?php }?>
    </div>
    </div>
    <div class="center"><div class="add_from_btn"><input type="button" value="Add Form" class="add_form"></div></div>
    <div class="orenge">
      <div class="left">
        <h3>complete your profile with a passport format picture and your contact information!</h3>
        <p>To complete the 1st step of the registration process, upload a professional photo and do not forget to provide us with contact information; we will e-mail you an automatically issued login password and contact you for the documents needed at the end of the registration process. </p>
      </div>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/vartical-seprater.png" class="vartical-strip">
      <div class="right">
        <h3>upload your passport size photo </h3>
        <figure class="arrow"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/right-solid-arrow.png"></figure>
        <figure class="camera"><a href="#" class="select_img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/camera.png"></a></figure>
       <div style="display:none;"> <input type="file" name="my_image_upload" id="my_image_upload"  multiple="false" /></div>
        <input type="email" name="email" placeholder="write your email address here... " class="require" id="email_id" value="<?php echo $user_info->user_email?>">
        <input type="text" name="mobile_no" placeholder="write your phone number here... " class="require" value="<?php echo get_user_meta( $user_id, 'mobile_no', $single )?>">
      </div>
    </div>

  <div class="sub-footer">
    <div class="inner">
      <a class="nxt-step" href="#step2" data-value="2">step 2<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/right-solid-arrow.png"></a>
      <!--<div class="edit-strip">View / edit profile<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/edit-symbol.png"></div>-->
      <button class="save" name="register_user">save<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/save-lock.png"></button>
    </div>
  </div>
</div>
</div>

<!-- step-2 -->
<div id="step2" class="hide_tab">
<div class="reg-step2">
	<div class="sub-header">
		<div class="step-penal">
			<div class="inactive"><a href="#step1" class="next_tab" data-value="1"><h5><span>step 1</span> out of 5</h5><p>Getting to know you</p></a></div>
			<div class="active"><a href="#step2" class="next_tab" data-value="2"><h5><span>step 2</span> out of 5</h5><p>Tell us more </p></a></div>
			<div class="inactive"><a href="#step3" class="next_tab" data-value="3"><h5><span>step 3</span> out of 5</h5><p>Your sALes video</p></a></div>
			<div class="inactive"><a href="#step4" class="next_tab" data-value="4"><h5><span>step 4</span> out of 5</h5><p>Check list</p></a></div>
			<div class="inactive"><a href="#step5" class="next_tab"  data-value="5"><h5><span>step 5</span> out of 5</h5><p>Online interview</p></a></div>
		</div>
	</div>
	
	<div class="form">
	<?php global $wpdb;
	$results = $wpdb->get_results( 'SELECT * FROM ej_questions where status=1 ORDER BY id DESC', OBJECT );

	$que_answer = unserialize(get_user_meta( $user_id, 'que_answer', $single ));
	
	$i = 1;
			foreach ( $results as $result ) 
			{
				echo '<div class="line"><div class="inner">
						<h3>'.$i.'. '.$result->question.'</h3>
						<input type="text" name="que_answer['.$result->id.']" class="char_limit require" placeholder="type your answer here..." maxlength="150" value="'.$que_answer[$result->id].'">
							<span class="limit"><span>0</span> / 150 ( character limit)</span>
						</div></div>';
						$i++;
			}
	?>
		<div class="line last"><div class="inner">
			<p>If you pass all the stages, when will you become a 5 star sales and start the new experience?</p>
			<select name="new_experience">
            	<option <?php echo get_user_meta( $user_id, 'new_experience', $single ) == '1-2 weeks' ? 'selected="selected"' : ''?>>1-2 weeks</option>
                <option <?php echo get_user_meta( $user_id, 'new_experience', $single ) == '3-5 weeks' ? 'selected="selected"' : ''?>>3-5 weeks</option></select>
		</div></div>
	</div>
	
	<div class="sub-footer">
		<div class="inner">
			<a class="nxt-step" href="#step3" data-value="3">step 3<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/right-solid-arrow.png"></a>
            <div class="edit-strip">View / edit profile<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/edit-symbol.png"></div>
      <button class="save" name="register_user">save<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/save-lock.png"></button>
		</div>
	</div>
</div>
</div>

<!-- step-3 -->
<div id="step3" class="hide_tab">
<div class="reg-step3">
	<div class="sub-header">
		<div class="step-penal">
			<div class="inactive"><a href="#step1" class="next_tab" data-value="1"><h5><span>step 1</span> out of 5</h5><p>Getting to know you</p></a></div>
			<div class="inactive"><a href="#step2" class="next_tab" data-value="2"><h5><span>step 2</span> out of 5</h5><p>Tell us more </p></a></div>
			<div class="active"><a href="#step3" class="next_tab" data-value="3"><h5><span>step 3</span> out of 5</h5><p>Your sALes video</p></a></div>
			<div class="inactive"><a href="#step4" class="next_tab" data-value="4"><h5><span>step 4</span> out of 5</h5><p>Check list</p></a></div>
			<div class="inactive"><a href="#step5" class="next_tab"  data-value="5"><h5><span>step 5</span> out of 5</h5><p>Online interview</p></a></div>
		</div>
	</div>
	
	<div class="innerbox">
	<h2>Upload a video of you selling a cosmetic product</h2>
		<div class="sellingtip">
			<h4>selling tips</h4>
			<p>The one skill which will open all doors for you, in ANY industry, is the ability to sell. If you get the ABC of it down and you can naturally back up everything you are taught in this respect, you have a shining and profitable career in front of you.</p>
			<a href="http://passingthroughjobs.com/wp-content/uploads/2015/10/step.pdf" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/read-more-left-symbol.png"></a>
		</div>
		<div class="exv">
			<h4>Example VIdeo</h4>
			<div class="video">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/two-girl-vedio.png">
			<a href="#ex_video" class="poplate"><button type="button"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/play-button.png"></button></a>
            <div style="display:none;">
            <div id="ex_video">
            	<div class="padding_30">
            	<iframe width="560" height="315" src="https://www.youtube.com/embed/bn8oOtl6MD4" frameborder="0" allowfullscreen=""></iframe>
                </div>
            </div>
            </div>
			</div>
			
		</div>
		<div class="uploadvid"><div class="inner">
			<h3>upload <span>here</span> your video of your 5 star Selling Technique</h3>
			<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bottom-solid-arrow.png"></figure>
			<figure class="upload_video"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/upload-video.png"><input type="file" name="upload_video" class="video_file"/></figure>
            
		</div></div>
	</div>
	
	<div class="sub-footer">
		<div class="inner">
			<a class="nxt-step" href="#step4" data-value="4">step 4<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/right-solid-arrow.png"></a>	
            <div class="edit-strip">View / edit profile<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/edit-symbol.png"></div>
      <button class="save" name="register_user">save<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/save-lock.png"></button>		
		</div>
	</div>
</div>
</div>

<!-- step-4 -->
<div id="step4" class="hide_tab">
<div class="reg-step4">
	<div class="sub-header">
		<div class="step-penal">
			<div class="inactive"><a href="#step1" class="next_tab" data-value="1"><h5><span>step 1</span> out of 5</h5><p>Getting to know you</p></a></div>
			<div class="inactive"><a href="#step2" class="next_tab" data-value="2"><h5><span>step 2</span> out of 5</h5><p>Tell us more </p></a></div>
			<div class="inactive"><a href="#step3" class="next_tab" data-value="3"><h5><span>step 3</span> out of 5</h5><p>Your sALes video</p></a></div>
			<div class="active"><a href="#step4" class="next_tab" data-value="4"><h5><span>step 4</span> out of 5</h5><p>Check list</p></a></div>
			<div class="inactive"><a href="#step5" class="next_tab"  data-value="5"><h5><span>step 5</span> out of 5</h5><p>Online interview</p></a></div>
		</div>
	</div>
	
	<div class="inner_block">
    <?php global $wpdb;
	$results = $wpdb->get_results( 'SELECT * FROM ej_question_step_4 where status=1 ORDER BY id DESC', OBJECT );
	$que_check = unserialize(get_user_meta( $user_id, 'que_check', $single ));
	
	$i = 1;
			foreach ( $results as $result ) 
			{
				echo '<div class="block">';?>
						   <span class="overcheckbox require2 <?php echo $que_check[$result->id]== 1 ? 'check' : ''?>">
                           	<input type="checkbox" name="que_check[<?php echo $result->id?>]" value="1" <?php echo $que_check[$result->id]== 1 ? 'checked="checked"' : ''?>></span>
                            <h4><?php echo $result->question?></h4><button type="button">view here</button> <?php echo '
						   <div class="hide que_detail">
						   		<p>'.$result->que_detail.'</p>
						   </div>
					  </div>';
			}
	?>
</div>
	<div class="sub-footer">
		<div class="inner">
			<a class="nxt-step" href="#step5" data-value="5">step 5<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/right-solid-arrow.png"></a>
      <div class="edit-strip">View / edit profile<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/edit-symbol.png"></div>
      <button class="save" name="register_user">save<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/save-lock.png"></button>
		</div>
	</div>
</div>
</div>

<!-- step-5 -->
<div id="step5" class="hide_tab">
<div class="reg-step5">
	<div class="sub-header">
		<div class="step-penal">
			<div class="inactive"><a href="#step1" class="next_tab" data-value="1"><h5><span>step 1</span> out of 5</h5><p>Getting to know you</p></a></div>
			<div class="inactive"><a href="#step2" class="next_tab" data-value="2"><h5><span>step 2</span> out of 5</h5><p>Tell us more </p></a></div>
			<div class="inactive"><a href="#step3" class="next_tab" data-value="3"><h5><span>step 3</span> out of 5</h5><p>Your sALes video</p></a></div>
			<div class="inactive"><a href="#step4" class="next_tab" data-value="4"><h5><span>step 4</span> out of 5</h5><p>Check list</p></a></div>
			<div class="active"><a href="#step5" class="next_tab"  data-value="5"><h5><span>step 5</span> out of 5</h5><p>Online interview</p></a></div>
		</div>
	</div>
	
	<div class="innerbox">
	<h3>Prepare for the final stage of your 5<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/5star.png"> application to live and work abroad
Soon you are going to be invited for an interview with one of our partner brands,
depending on your application preferences.</h3>
		<div class="block">
			<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/watch.png"></figure>
			<h4>Respect the time and date!</h4>
			<p>There is a clear advantage to having an online interview: you do not have to leave your house and make it somewhere on time. However, this does not make it any less important than other interviews you may go to. There is no excuse for being late!</p>
		</div>
		<div class="block">
			<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/dress-well.png"></figure>
			<h4>You still have to dress to impress!</h4>
			<p>The business casual dress code is the best choice. Remember! You are applying for a job in cosmetics sales and appearance matters twice as much in this case. Be proper and well groomed.</p>
		</div>
		<div class="block">
			<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/laptyop.png"></figure>
			<h4>Double check your Internet connection!</h4>
			<p>Make sure your Internet connection is good and it can hold good quality video streaming.</p>
		</div>
	</div>
	
	<div class="orenge">
		<div class="inner">
			<h3>Please check your email regularly to see your interview schedule and login details.</h3>
		</div>
	</div>
    <!--<div class="sub-footer">
		<div class="inner">
			<button class="save" name="register_user">save<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/save-lock.png"></button>
		</div>
	</div>-->
</div>
</div>
<div style="display:none">
<input type="submit" name="register_user" value="register user" id="register_user"/>
</div>
</form>
<?php 
	return "";
}
add_shortcode( 'register_from', 'int_register_from' );
