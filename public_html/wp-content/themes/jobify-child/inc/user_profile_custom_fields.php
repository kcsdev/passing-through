<?php
// Hooks near the bottom of profile page (if current user) 
add_action('show_user_profile', 'custom_user_profile_fields');

// Hooks near the bottom of the profile page (if not current user) 
add_action('edit_user_profile', 'custom_user_profile_fields');
// @param WP_User $user
function custom_user_profile_fields( $user ) {
?>

<h3>Person 1</h3>
<table class="form-table">
  <tr>
    <th> <label for="code">
        <?php _e( 'Phone Number:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'mobile_no', $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'Age:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'age', $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'Gender:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'gender2', $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'English level:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'english', $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'Second language:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'scndlang', $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'Level:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'level', $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'City of Residence:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'city', $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'Last school degree:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'schooldegree', $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'What did you study?' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'study_type', $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'Work experience:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'exp', $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'Experience In sales:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'expinsel', $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'What position?:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'position', $user->ID ) ); ?></td>
  </tr>
</table>
<?php $person = get_the_author_meta( 'form_id', $user->ID );
	  $no_person = unserialize($person);
	  if(!empty($no_person)){
	  	$preserved = array_reverse($no_person);
	  
	  
 for($i=0; $i<sizeof($preserved); $i++){

 ?>
<h3>Person <?php echo $i+2;?></h3>
<table class="form-table">
  <tr>
    <th> <label for="code">
        <?php _e( 'Fullname:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'fullname'.$no_person[$i], $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'Age:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'age'.$no_person[$i], $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'Gender:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'gender'.$no_person[$i], $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'English level:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'english'.$no_person[$i], $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'Second language:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'scndlang'.$no_person[$i], $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'Level:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'level'.$no_person[$i], $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'City of Residence:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'city'.$no_person[$i], $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'Last school degree:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'schooldegree'.$no_person[$i], $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'What did you study?' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'study_type'.$no_person[$i], $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'Work experience:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'exp'.$no_person[$i], $user->ID ) ); ?></td>
  </tr>
  <tr>
    <th> <label for="code">
        <?php _e( 'Experience In sales:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'expinsel'.$no_person[$i], $user->ID ) ); ?></td>
    <th> <label for="code">
        <?php _e( 'What position?:' ); ?>
      </label>
    </th>
    <td><?php echo esc_attr( get_the_author_meta( 'position'.$no_person[$i], $user->ID ) ); ?></td>
  </tr>
</table>
<?php
}
	  }

$que_answer = unserialize(get_the_author_meta( 'que_answer', $user->ID ));
global $wpdb;

if(!empty($que_answer)): // start Tell us more
?>
<table class="form-table user_data">
  <h3>Tell us more</h3>
  <?php 
	foreach($que_answer as $key => $val):
		$que = $wpdb->get_row($wpdb->prepare("SELECT question FROM ".$wpdb->prefix."questions where status = 1 and id = %d", $key));
		if(!empty($que)){?>
          <tr>
            <td class="que"> <?php _e( $que->question ); ?></td>
          </tr>
          <tr class="ans">
            <td>Answer: <?php _e( $val ); ?></td>
          </tr>
          <?php }
	endforeach; ?>
</table>
<style>
.user_data {background:#fff;}
.user_data td{padding-top: 5px;}
.user_data .que{font-weight: bold; padding-top: 15px; padding-bottom: 0px;}
.user_data .ans{border-bottom: 1px solid #E0E0E0;}
</style>
<?php 
endif; // end Tell us more

$que_check = unserialize(get_the_author_meta( 'que_check', $user->ID ));
global $wpdb;
if(!empty($que_check)): // start Check list
?>
<table class="form-table user_data">
  <h3>User Check list</h3>
  <?php 
	foreach($que_check as $key => $val):
		$ques = $wpdb->get_row($wpdb->prepare("SELECT question FROM ".$wpdb->prefix."question_step_4 where status = 1 and id = %d", $key));
		if(!empty($ques)){?>
          <tr>
            <td class="que"> <?php _e( $ques->question ); ?></td>
          </tr>
          <tr class="ans">
            <td>Answer: <?php echo $val == 1 ? 'Yes' : 'No' ?></td>
          </tr>
          <?php }
	endforeach; ?>
</table>
<?php endif; //end Check list
}// custom_user?>
