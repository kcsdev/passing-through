<?php
function int_job_listing(){
	
	if($_REQUEST['orderbyjob'] == 'recommended'){
		$orderby = '_job_highlight';			
	}
	if($_REQUEST['orderbyjob'] == 'location'){
		$orderby = '_job_location';			
	}
	
	if($_REQUEST['orderbyjob'] == 'brand'){
		$orderby = '_job_brand';			
	}
	
	if($_REQUEST['orderbyjob'] == 'ex_lavel'){
		$orderby = '_ex_lavel';			
	}
	
	$args = array('post_type' => 'job_listing', 'meta_key' => $orderby, 'orderby' => 'meta_value', 'order' => 'ASC', 'post_status' => 'publish','posts_per_page' => get_option( 'job_manager_per_page' ));
	
	if($_REQUEST['orderbyjob'] == 'recommended'){
		$args = array('post_type' => 'job_listing', 'meta_key' => $orderby, 'meta_value' => 1, 'orderby' => 'meta_value', 'order' => 'ASC', 'post_status' => 'publish','posts_per_page' => get_option( 'job_manager_per_page' ));
	}
	if(empty($orderby)){
		$args = array('post_type' => 'job_listing', 'orderby' => 'post_date', 'order' => 'DESC', 'post_status' => 'publish','posts_per_page' => get_option( 'job_manager_per_page' ));
	}
	
	$query = new WP_Query($args);
	echo '<div class="job_listings">';
	get_job_manager_template( 'job-listings-start.php' );
	
	while ( $query->have_posts() ) : $query->the_post();?>

		<?php get_job_manager_template_part( 'content', 'job_listing_custom' ); ?>

	<?php endwhile; ?>
    
	<?php if($_REQUEST['orderbyjob'] == 'recommended'){ // if display recommended job
			$args2 = array('post_type' => 'job_listing', 'meta_key' => $orderby, 'meta_value' => 0, 'orderby' => 'meta_value', 'order' => 'ASC', 'post_status' => 'publish','posts_per_page' => get_option( 'job_manager_per_page' ));
			$query2 = new WP_Query($args2);
			while ( $query2->have_posts() ) : $query2->the_post();?>

		<?php get_job_manager_template_part( 'content', 'job_listing_custom' ); ?>

	<?php endwhile; ?>
    	
    <?php }// end recommended job?>
    
	<?php get_job_manager_template( 'job-listings-end.php' ); ?>
  
<?php
echo '</div>';
}
add_shortcode( 'job_listing', 'int_job_listing' );