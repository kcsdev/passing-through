<?php

wp_enqueue_script('jquery');

function job_listing_ajex(){

global $wpdb;

 $orderby = $_POST['fruit'];

	$args = array('post_type' => 'job_listing', 'meta_key' => $orderby, 'orderby' => 'meta_value', 'order' => 'ASC', 'post_status' => 'publish','posts_per_page' => get_option( 'job_manager_per_page' ));
	
	if($orderby == '_job_highlight'){
		$args = array('post_type' => 'job_listing', 'meta_key' => $orderby, 'meta_value' => 1, 'orderby' => 'meta_value', 'order' => 'ASC', 'post_status' => 'publish','posts_per_page' => get_option( 'job_manager_per_page' ));
	}
	
	$query = new WP_Query($args);
	
	get_job_manager_template( 'job-listings-start.php' );
	
	while ( $query->have_posts() ) : $query->the_post();
		
		get_template_part( 'content', 'job_listing_custom_ajex' ); 
		
	endwhile; 
	
	if($orderby == '_job_highlight'){ // if display recommended job
			$args2 = array('post_type' => 'job_listing', 'meta_key' => $orderby, 'meta_value' => 0, 'orderby' => 'meta_value', 'order' => 'ASC', 'post_status' => 'publish','posts_per_page' => get_option( 'job_manager_per_page' ));
			$query2 = new WP_Query($args2);
			while ( $query2->have_posts() ) : $query2->the_post();

		get_job_manager_template_part( 'content', 'job_listing_custom_ajex' ); 

		endwhile; 
	}
	get_job_manager_template( 'job-listings-end.php' );
	
die();
}
add_action('wp_ajax_job_listing_ajex', 'job_listing_ajex');
add_action('wp_ajax_nopriv_job_listing_ajex', 'job_listing_ajex'); // not really needed