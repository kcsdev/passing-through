<?php 
add_action( 'admin_menu', 'theme_option_init' );
 
 function theme_option_init()
 {
	 register_setting('signup_ques', 'signup_ques_options');
	 add_submenu_page( 'edit.php?post_type=job_listing','Sign Up Setting ', 'Sign Up Setting', 'edit_theme_options', 'signup_ques', 'signup_ques_page');	
 }
 
 function signup_ques_page() {?>
<?php global $wpdb;
// step 2
if(isset($_POST['add_que'])){
	$wpdb->insert( 'ej_questions', array( 'question' => $_POST['add_que_db'], 'status' => 1 ), array( '%s', '%d' ) );
	$msg = 'Question added';
}
if(isset($_POST['edit_que'])){
	
	$wpdb->update( 'ej_questions', array( 'question' => $_POST['edit_que_db']), array( 'ID' => $_POST['que_id'] ), array( '%s'), array( '%d') );
	$msg = 'Question updated';
}
if(isset($_GET['delete'])){
	//$wpdb->delete( 'ej_questions', array( 'ID' => $_GET['delete'] ), array( '%d' ) );
	$wpdb->update( 'ej_questions', array( 'status' => 0 ), array( 'ID' => $_GET['delete'] ), array( '%d'), array( '%d') );
	$msg = 'Question Deleted';

}
?>
<?php // step 4 
if(isset($_POST['add_que_st4'])){
	$wpdb->insert( 'ej_question_step_4', array( 'question' => $_POST['add_que_db_st4'], 'que_detail' => $_POST['add_que_detail'], 'status' => 1 ), array( '%s', '%s', '%d' ) );
	$msg = 'Question added';
}
if(isset($_POST['edit_que_st4'])){
	
	$wpdb->update( 'ej_question_step_4', array( 'question' => $_POST['edit_que_db_st4'], 'que_detail' => $_POST['edit_que_detail']), array( 'ID' => $_POST['que_id_st4'] ), array( '%s', '%s'), array( '%d') );
	$msg = 'Question updated';
}
if(isset($_GET['delete_st4'])){
	//$wpdb->delete( 'ej_question_step_4', array( 'ID' => $_GET['delete'] ), array( '%d' ) );
	$wpdb->update( 'ej_question_step_4', array( 'status' => 0 ), array( 'ID' => $_GET['delete_st4'] ), array( '%d'), array( '%d') );
	$msg = 'Question Deleted';

}
?>
<?php if(!empty($msg)):?>
<div id="message" class="updated notice notice-success is-dismissible below-h2"><p><?php echo $msg?>.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
<?php endif;?>
<div class="right_part">
  <h2 class="nav-tab-wrapper"> <a href="#step2" class="nav-tab nav-tab-active">Step 2 Questions</a> <a href="#step4" class="nav-tab" id="tab_2">Step 4 Questions</a> </h2>
  <div id="step2" class="settings_panel">
    <h3>Step 2 Questions</h3></td>
    <table class="with_border">
      <?php 
			$results = $wpdb->get_results( 'SELECT * FROM ej_questions where status=1 ORDER BY id DESC', OBJECT );
			foreach ( $results as $result ) 
			{
				echo '<tr>
						<td width="40px">'.$result->id.'</td>
						<td width="80%"><a href="edit.php?post_type=job_listing&page=signup_ques&edit='.$result->id.'">'.$result->question.'</a></td>
						<td width="40px"><a href="edit.php?post_type=job_listing&page=signup_ques&delete='.$result->id.'"">Delete</a></td>
					 </tr>';
			}?>
    </table>
	<?php if(isset($_GET['edit'])){
		$question = $wpdb->get_var( "SELECT question FROM ej_questions where id=".$_GET['edit'] );?>
	<form method="post" style="margin-top:20px;    margin-left: -10px;" action="edit.php?post_type=job_listing&page=signup_ques">
    	<table>
        <tr>
          <td><textarea name="edit_que_db"><?php echo $question;?></textarea></td>
        </tr>
        <tr>
          <td>
          <input type="hidden" value="<?php echo $_GET['edit']?>" name="que_id" />
          	<input type="submit" name="edit_que" value="Edit Question" class="button-primary"/>
           
           </td>
        </tr>
      </table>
    </form>
	<?php }
	else{?>
    <form method="post" action="edit.php?post_type=job_listing&page=signup_ques" style="margin-top:20px;    margin-left: -10px;">
      <table>
        <tr>
          <td><textarea name="add_que_db" placeholder="Enter Question"></textarea></td>
        </tr>
        <tr>
          <td>
          <input type="submit" name="add_que" value="Add Question" class="button-primary"/>
		    
           </td>
        </tr>
      </table>
    </form>
    <?php }?>
    
  </div>
  
  <div id="step4" class="settings_panel" style="display:none;">
    <h3>Step 4 Questions</h3>
	<table class="with_border">
      <?php 
			$results = $wpdb->get_results( 'SELECT * FROM ej_question_step_4 where status=1 ORDER BY id DESC', OBJECT );
			foreach ( $results as $result ) 
			{
				echo '<tr>
						<td width="40px">'.$result->id.'</td>
						<td width="80%"><a href="edit.php?post_type=job_listing&page=signup_ques&edit_st4='.$result->id.'">'.$result->question.'</a></td>
						<td width="40px"><a href="edit.php?post_type=job_listing&page=signup_ques&delete_st4='.$result->id.'"">Delete</a></td>
					 </tr>';
			}?>
    </table>
	<?php if(isset($_GET['edit_st4'])){
		$question = $wpdb->get_var( "SELECT question FROM ej_question_step_4 where id=".$_GET['edit_st4'] );
		$question_detail = $wpdb->get_var( "SELECT que_detail FROM ej_question_step_4 where id=".$_GET['edit_st4'] );?>
	<form method="post" style="margin-top:20px;    margin-left: -10px;" action="edit.php?post_type=job_listing&page=signup_ques">
    	<table>
        <tr>
          <td><textarea name="edit_que_db_st4"><?php echo $question;?></textarea></td><td><textarea name="edit_que_detail"><?php echo $question_detail;?></textarea></td>
        </tr>
        <tr>
          <td>
          <input type="hidden" value="<?php echo $_GET['edit_st4']?>" name="que_id_st4" />
          	<input type="submit" name="edit_que_st4" value="Edit Question" class="button-primary"/>
           
           </td>
        </tr>
      </table>
    </form>
	<?php }
	else{?>
    <form method="post" action="edit.php?post_type=job_listing&page=signup_ques" style="margin-top:20px;    margin-left: -10px;">
      <table>
        <tr>
          <td><textarea name="add_que_db_st4" placeholder="Enter Question"></textarea></td><td><textarea name="add_que_detail" placeholder="Enter Question Detail"></textarea></td>
        </tr>
		
        <tr>
          <td>
          <input type="submit" name="add_que_st4" value="Add Question" class="button-primary"/>
		    
           </td>
        </tr>
      </table>
    </form>
    <?php }?>
  </div>
</div>
<script type="text/javascript">
			jQuery('.nav-tab-wrapper a').click(function() {
				jQuery('.settings_panel').hide();
				jQuery('.nav-tab-active').removeClass('nav-tab-active');
				jQuery( jQuery(this).attr('href') ).show();
				jQuery(this).addClass('nav-tab-active');
				return false;
			});
			<?php if(isset($_GET['edit_st4'])){?>
				jQuery('#tab_2').click();
			<?php }?>
		</script>
        <style>
		.with_border{ border: 1px solid #ccc;  background-color: #fff; padding: 16px;}
		td{padding:10px;}
		tr:nth-child(even){background-color: #F5F5F5;}
		textarea{width:400px; height:100px;padding:10px;}
		a{text-decoration:none; font-size:14px;}
		</style>
<?php }


