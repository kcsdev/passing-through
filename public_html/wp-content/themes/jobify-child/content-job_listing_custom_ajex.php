<?php global $post; ?>
<?php $highlight = get_post_meta( get_the_ID(), '_job_highlight', true );
if($highlight == 1){
	$class = 'highlight';
}
else{
	$class= '';
}?>
<?php if ( $apply = get_the_job_application_method() ) :

	if ( $apply->type === 'url' ) {
	    $application_href = $apply->url;
	} elseif ( $apply->type === 'email' ) {
	    $application_href = sprintf( 'mailto:%1$s%2$s', $apply->email, '?subject=' . rawurlencode( $apply->subject )  );
	}
	endif;
	?>
<?php $fields = get_fields(get_the_ID());
//print_r($fields);?>    
<li <?php job_listing_class("$class"); ?> data-longitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_long ); ?>">
		<?php $salary = get_post_meta( get_the_ID(), '_job_location', true );?>
		<div class="position">
			<h3><?php the_title(); ?></h3>
            <a class="read_more" href="#inline_content<?php echo $post->ID?>">READ MORE</a>
		</div>

		<div class="location">
        	<label>Location:</label>
            <div class="job_rs">
			<?php //the_job_location( ); ?>
            <?php echo get_post_meta( get_the_ID(), '_job_location', true );?>
            </div>
		</div>
		<div class="brand">
        	<label>Brand:</label>
            <div class="job_rs"><?php echo get_post_meta( get_the_ID(), '_job_brand', true );?></div>
        </div>
        <div class="application">
        <div class="job_application">

			<?php //do_action( 'job_application_start', $apply ); ?>
            
            <input type="button" class="application_button button" value="<?php _e( 'Apply Now', 'wp-job-manager' ); ?>" href="<?php echo esc_url( get_permalink(get_the_ID()) ); ?>"/>
            
            <div class="application_details">
                <?php
                    /**
                     * job_manager_application_details_email or job_manager_application_details_url hook
                     */
                    //do_action( 'job_manager_application_details_' . $apply->type, $apply );
                ?>
            </div>
            <?php //do_action( 'job_application_end', $apply ); ?>
        </div>
		</div>
        <div class="comp_log">
        <?php the_company_logo(); ?>
        </div>
</li>
