<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package Jobify
 * @since Jobify 1.0
 */
?>

		
<?php if ( jobify_theme_mod( 'jobify_cta', 'jobify_cta_display' ) ) : ?>
		<div class="footer-cta">
			<div class="container">
				<?php echo wpautop( jobify_theme_mod( 'jobify_cta', 'jobify_cta_text' ) ); ?>
			</div>
		</div>
		<?php endif; ?>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( is_active_sidebar( 'widget-area-footer' ) ) : ?>
			<div class="footer-widgets">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'widget-area-footer' ); ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<div class="copyright">
				<div class="container">
					<div class="site-info">
						<?php echo apply_filters( 'jobify_footer_copyright', sprintf( __( '&copy; %1$s %2$s &mdash; All Rights Reserved', 'jobify' ), date( 'Y' ), get_bloginfo( 'name' ) ) ); ?>
					</div><!-- .site-info -->

					<a href="#top" class="btt"><i class="icon-up-circled"></i></a>

					<?php
						if ( has_nav_menu( 'footer-social' ) ) :
							$social = wp_nav_menu( array(
								'theme_location'  => 'footer-social',
								'container_class' => 'footer-social',
								'items_wrap'      => '%3$s',
								'depth'           => 1,
								'echo'            => false,
								'link_before'     => '<span class="screen-reader-text">',
								'link_after'      => '</span>',
							) );

							echo strip_tags( $social, '<a><div><span>' );
						endif;
					?>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div>
    </div><!-- #page -->
    
        
   <!-- <p>This is the main content. To display a lightbox click <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">here</a></p>
        <div id="light" class="white_content"><p>This is the lightbox content.</p> <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a></div>
        <div id="fade" class="black_overlay"></div>-->
    
	<?php wp_footer(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.min.js" type="text/javascript"></script>
	<script>
	
$(document).ready(function(){
	var wd = $(window).width();
	if ((navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) && wd < 568) {
		//alert('test');
		//$('.application_button.button').css({'line-height':'26px', 'padding':'20px 20px 15px;'});
	}
});
function display_msg(){
	alert('teswt');
}
	</script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js" type="text/javascript"></script>
    
    <script>
		jQuery(document).on("click",".custom_filter ul li a", function(){
			jQuery('div.job_listings').prepend('<div class="loading_job"></div>');
			var filter = jQuery(this).data('ajax');		
			 jQuery.ajax({
			  type:"POST",
			  url: "<?php echo get_home_url(); ?>/wp-admin/admin-ajax.php", // url required full path......
			  data: {
			  'action':'job_listing_ajex',
			  'fruit' : filter
			  },
			  success:function(data){
			  	
				jQuery('.job_listings').html(data);
				//jQuery('.job_listings .loading_job').remove();
				$.mCustomScrollbar.defaults.theme="inset"; //set "inset" as the default theme
				$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
	            $("ul.job_listings").mCustomScrollbar();
				jQuery('.loc_gallary').slick({
					slidesToShow: 4,
					slidesToScroll: 1,
					autoplay: true,
					autoplaySpeed: 2000,
				});
			  }
		  }); 
		  return false;
        });
		
	(function($){
				
				$.mCustomScrollbar.defaults.theme="inset"; //set "inset" as the default theme
				$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
				
				/*$("body").mCustomScrollbar({
					axis:"yx",
					scrollbarPosition:"outside"
				});*/
				
				$("#main,#page,body , .box_gray,.brand_list, .support_tab, ul.job_listings, .reg-step4 .inner_block").mCustomScrollbar();
				
		})(jQuery);
		$(window).load(function(e) {
			$.mCustomScrollbar.defaults.theme="inset"; //set "inset" as the default theme
			$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
            $("ul.job_listings").mCustomScrollbar();
        });
		
		<?php if(isset($_REQUEST['orderbyjob'])){?>
			jQuery(document).ready(function(e) {
                jQuery('.sin_line:first .tab_title a').click();
            });
				
		<?php }?>
	</script>
   
</body>
</html>